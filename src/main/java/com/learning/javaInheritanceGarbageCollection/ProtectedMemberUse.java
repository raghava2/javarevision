package com.learning.javaInheritanceGarbageCollection;

public class ProtectedMemberUse {
    public static void main(String[] args) {
        Parent c = new Parent();
        System.out.println(c.def);
        System.out.println(c.pro);
    }

}
