package com.learning.javaInheritanceGarbageCollection;

public abstract interface IntrfaceLearnable {
    public abstract void canLearn();
    public static final int SCORE_AFTER_LEARN = 100;

    static int getScoreAfterLearn(){
        return 100;
    }

    default int scoreWhileLearning(){
        try {
            Thread.sleep(20);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return 100;
    }
}