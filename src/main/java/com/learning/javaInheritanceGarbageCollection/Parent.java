package com.learning.javaInheritanceGarbageCollection;

public class Parent implements IntrfaceLearnable {

    protected int pro = 1;
    int def = 2;

    public String var = "Raghav";

    private void setVar(String var) {
        var = var;
        System.out.println(var);
    }

    public String getName(final String aString){
//        aString = aString + "Raghav";   //final argument can't be reassigned in the method
        return aString;
    }
    @Override
    public void canLearn() {
        String abilityToLearn = "cent per cent";

    }

    @Override
    public int scoreWhileLearning() {
        System.out.println("This is us learning at our best");
        return IntrfaceLearnable.super.scoreWhileLearning();
    }


    public static void main(String[] args) {
        System.out.println(new Parent().scoreWhileLearning());
        System.out.println(IntrfaceLearnable.SCORE_AFTER_LEARN);
        System.out.println(new Parent().getName("Rags"));
//        new LearnNow().setVar("Ksv");
    }
}
