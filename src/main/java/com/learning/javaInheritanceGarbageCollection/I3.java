package com.learning.javaInheritanceGarbageCollection;

public interface I3 extends I2, IntrfaceLearnable{

    default float methodName(){
        double c =(float)1;
        return (float)c; //explicit cast not required
    }
}
