package com.learning.javaInheritanceGarbageCollection;

public class GarbageCollectionRuntimeThread {

    GarbageCollectionRuntimeThread[] a = new GarbageCollectionRuntimeThread[1000];
    GarbageCollectionRuntimeThread x;
    public static void main(String[] args) {
        Runtime rt = Runtime.getRuntime();
        System.out.println(rt.totalMemory());
        System.out.println(rt.freeMemory());

        GarbageCollectionRuntimeThread x = new GarbageCollectionRuntimeThread();

        for (int i =0;i<1000;i++){
            x.a[i] = new GarbageCollectionRuntimeThread();
        }

        Runtime rt1 = Runtime.getRuntime();
        System.out.println(rt1.totalMemory());
        System.out.println(rt1.freeMemory());

        System.gc();

        Runtime rt2 = Runtime.getRuntime();
        System.out.println(rt2.totalMemory());
        System.out.println(rt2.freeMemory());

    }
}