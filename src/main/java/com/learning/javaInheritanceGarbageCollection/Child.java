package com.learning.javaInheritanceGarbageCollection;



 public class Child extends Parent {
    private int setVar(int a) {

        setVar(String.valueOf(a));
        return a;
    }


    public void setVar(String var) {
        var = var;
        System.out.println(var);
    }
//    public double setVar(int var) { // can't just differ in return type of method, can differ in return type of params
    public int setVar(double var) {
    var = var;
        return (int) var;
    }
//    abstract int abint(); // abstract method only in abstract class


     private class Abc{ // only inner classes can be private, top level classes can only be public or default

     }

}
