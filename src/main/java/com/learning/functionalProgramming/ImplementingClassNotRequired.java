package com.learning.functionalProgramming;

public class ImplementingClassNotRequired implements CustomFunctionalInterfaceLikePredicateConsumer{

    @Override
    public boolean check() {
        return true;
    }
}
