package com.learning.functionalProgramming;

@FunctionalInterface
public interface CustomFunctionalInterfaceLikePredicateConsumer {

    boolean check();
}
