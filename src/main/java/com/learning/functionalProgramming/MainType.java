package com.learning.functionalProgramming;

import java.util.function.Function;

public class MainType {
    public static void main(String[] args) {
        CustomFunctionalInterfaceLikePredicateConsumer i = new ImplementingClassNotRequired();
        System.out.println(i.check());

        //SHORTCUT WAY OF implementing functionalinterface
        CustomFunctionalInterfaceLikePredicateConsumer i2 = new CustomFunctionalInterfaceLikePredicateConsumer() {
            @Override
            public boolean check() {
                return false;
            }
        };

        System.out.println(i2.check());

        CustomFunctionalInterfaceLikePredicateConsumer i3 = () -> true;
        System.out.println(i3.check());


        Function<Integer,Integer> triple = new Function<Integer, Integer>() {
            @Override
            public Integer apply(Integer integer) {
                return integer*3;
            }
        };

        System.out.println(triple.apply(50));
    }
}
