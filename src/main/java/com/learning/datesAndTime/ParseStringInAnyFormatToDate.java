package com.learning.datesAndTime;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.FormatStyle;

public class ParseStringInAnyFormatToDate {

        public static void main(String[] args) {
            String dateString = "2011-12-03+01:00"; // Replace with your input string

            // Define the input date formats to be parsed
            String[] dateFormats = {
                    "yyyy-MM-dd HH:mm:ss",
                    "yyyy-MM-dd'T'HH:mm:ss",
                    "dd/MM/yyyy HH:mm:ss",
                    "yyyy-MM-dd"

                    // Add more formats as needed
            };

            DateTimeFormatter[] dfs = {

                    DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT),
                    DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL),
                    DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG),
                    DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM),
                    DateTimeFormatter.BASIC_ISO_DATE,
                    DateTimeFormatter.ISO_LOCAL_DATE,
                    DateTimeFormatter.ISO_OFFSET_DATE,
                    DateTimeFormatter.ISO_DATE,
                    DateTimeFormatter.ISO_LOCAL_TIME,
                    DateTimeFormatter.ISO_OFFSET_TIME,
                    DateTimeFormatter.ISO_TIME,
                    DateTimeFormatter.ISO_LOCAL_DATE_TIME,
                    DateTimeFormatter.ISO_OFFSET_DATE_TIME,
                    DateTimeFormatter.ISO_ZONED_DATE_TIME,
                    DateTimeFormatter.ISO_DATE_TIME,
                    DateTimeFormatter.ISO_ORDINAL_DATE,
                    DateTimeFormatter.ISO_WEEK_DATE,
                    DateTimeFormatter.ISO_INSTANT,
                    DateTimeFormatter.RFC_1123_DATE_TIME
            };

            LocalDateTime parsedDateTime = null;

            int i=1;
            while (parsedDateTime ==null){
                    if (i==1){
                        parsedDateTime = checkWithPatterns(dateString,dateFormats);
                    }
                    if (i==2){
                        parsedDateTime = checkWithDateFormatters(dateString, dfs);

                    }
                    if (i==3){
                        break;
                    }
                i++;
            }


            if (parsedDateTime != null) {
                System.out.println("Parsed LocalDateTime: " + parsedDateTime);
            } else {

                System.out.println("Failed to parse the date string.");
            }



        }

    private static LocalDateTime checkWithDateFormatters(String dateString, DateTimeFormatter[] dfs) {

        LocalDateTime parsedDateTime =null;
        for (DateTimeFormatter format : dfs) {
            try {
                parsedDateTime = LocalDateTime.parse(dateString, format);
                System.out.println(format.toString());
                break;
            } catch (DateTimeParseException e) {
                // Parsing failed, try the next format
            }
        }
        return parsedDateTime;
        }

    private static LocalDateTime checkWithPatterns(String dateString, String[] dateFormats){
            LocalDateTime parsedDateTime =null;
            for (String format : dateFormats) {
                try {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
                    parsedDateTime = LocalDateTime.parse(dateString, formatter);
                    break;
                } catch (DateTimeParseException e) {
                    // Parsing failed, try the next format
                }
            }
            return parsedDateTime;
        }


    }


