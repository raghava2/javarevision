package com.learning.datesAndTime;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class DateTimeLearning {

//    For example:
//    LocalDate date = LocalDate.now();
//    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy MM dd");
//    String text = date.format(formatter);
//    LocalDate parsedDate = LocalDate.parse(text, formatter);

    public static void main(String[] args) {
        LocalDateTime now = LocalDateTime.now();
        LocalDate now123 = LocalDate.now(ZoneId.of("America/Los_Angeles"));
//        LocalDate n23 = LocalDate.now(Clock.tickMillis(ZoneId.of("America/Los_Angeles")));
        LocalDate n24 = LocalDate.now(Clock.systemUTC());

        System.out.println(n24.format(DateTimeFormatter.ofPattern("dd MMMM yyyy")));

        DateTimeFormatter x33 = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG);
//        DateTimeFormatter x33 = DateTimeFormatter.ofLocalizedDate(FormatStyle.valueOf("123"));

        System.out.println("LocalDate formatter"+n24.format(x33)); //format() of LocalDate class

//        System.out.println("DateTimeFormatter: "+x33.format(n23));

        LocalDate d1 = LocalDate.of(1992, Month.NOVEMBER, 30);
        LocalDate d12 = LocalDate.of(1992, 11, 30);

        LocalDate d23 = LocalDate.ofYearDay(1922,365);
        LocalDate d24 = LocalDate.ofEpochDay(1922); // This means 1922 days after 1970-01-01



        Set<String> s1 = ZoneId.getAvailableZoneIds();

        List<String> zoneIds = new ArrayList<>(s1);
        int n = zoneIds.size();
        String zone = zoneIds.get( (int) (Math.random()*n));
        LocalDateTime now1= LocalDateTime.now(ZoneId.of("America/Los_Angeles"));

        now1 = LocalDateTime.now(ZoneId.of(zone));

//        List<String> zoneIds = Arrays.asList(s1.forEach(String a-> return a;));
        s1.forEach( a -> System.out.print(a.toString()+ " "));

        System.out.println("old"+d1);

        Period d1m2 = Period.of(0,2,1); //Period.ofDays(1)+Period.ofMonths(2);

        Period w1 = Period.parse("P1W");
        Period m2 = Period.parse("P2M");
        d1 = d1.plus(d1m2);

        d1 = d1.plus(w1);
        d1 = d1.plus(m2);

        Duration d1s = Duration.of(6000, ChronoUnit.SECONDS);

        System.out.println("added d1m2"+d1);


        d1 = d1.plusDays(1);


        d1 = d1.plus(1, ChronoUnit.DAYS);
        d1 = d1.plus(1, ChronoUnit.MONTHS);



        System.out.println(now);
        System.out.println("now1 in "+zone+": "+now1);
        System.out.println(d1);

        DateTimeFormatter df = DateTimeFormatter.ISO_DATE_TIME;
        now.format(df);

        System.out.println(now);

        LocalDate parsedDate = LocalDate.parse("1992-11-30");
        parsedDate = LocalDate.parse("13-05-1992", DateTimeFormatter.ofPattern("dd-MM-yyyy"));
//        parsedDate = LocalDate.parse("",DateTimeFormatter.BASIC_ISO_DATE);
        System.out.println(parsedDate.getMonth());


        LocalTime time = LocalTime.now();
        System.out.println(time);

        LocalDateTime datetime = LocalDateTime.now();
        System.out.println(datetime);

        Period hour = Period.ofMonths(3);

    }
}
