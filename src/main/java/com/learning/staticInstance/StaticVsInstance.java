package com.learning.staticInstance;

public class StaticVsInstance {


    int x;

    public static void main(String[] args) {

    }

    static void func1(){

        new StaticVsInstance().func2(); //we can call instance method and variable from static method but only with object reference and dot

        int y = new StaticVsInstance().x;
//        func2();

    }

    void func2(){
        func1();
        StaticVsInstance x = null;
        func1();
    }


}

