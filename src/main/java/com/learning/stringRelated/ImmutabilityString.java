package com.learning.stringRelated;

import java.nio.charset.StandardCharsets;

public class ImmutabilityString {

    public static void main(String[] args) {
        String x = new String("Abcd1");
        String x1 = "Abcd"; //both are the same only difference is in short representation

        x1.concat("Java");
//        creates a new String "AbcdJava but as it's not being pointed by any reference, so it's instantly lost

        System.out.println(x1); //value of x1 remains the same Abcd

        x = x1.concat(x); // "AbcdAbcd1" new string created and x1 now points to this new string, original Abcd1 becomes eligible for garbage collection

        System.out.println(x);

        //For making changes to String we have 2 classes StringBuilder and StringBuffer
        //Both of these classes are the same with same methods but only difference is that StringBuilder is faster as it's not synchronized
        StringBuilder str = new StringBuilder(x);
        System.out.println(x.length());
        System.out.println(x.concat("AddingStringBuffer"));
        System.out.println(x.getBytes(StandardCharsets.UTF_8));
        System.out.println(str.insert(2,3.5));
        System.out.println(str.insert(2,"2offsetishere"));
        System.out.println(str.charAt(1));
        System.out.println(x.charAt(0));
        System.out.println(x.substring(0));
        System.out.println(x = x.substring(1));
        System.out.println(x = "A".concat(x));

        System.out.println(str);
        System.out.println(str.lastIndexOf("A"));
        System.out.println(str.lastIndexOf("."));

        System.out.println(str);
        System.out.println(str.substring(1,3));
        System.out.println(str.replace( 1, 3, "Raghav"));

        System.out.println(x);
        System.out.println(x.substring(1,3));
        System.out.println(x.replace( "A", "Raghav"));

        x.equalsIgnoreCase("x");


        System.out.println(x);
        String s = new String(x.toCharArray(), 2,5);
        System.out.println(s);



        int[] x12 = {65,66,67,68,69,70,71,2,3,4,5,6};
//        System.out.println(Arrays.stream(x12).collect(r -> { return r>0}).toString().toString());
         s = new String(x12, 1,1);
        System.out.println(s);

        System.out.println(str);
        System.out.println(str.length());
        str.append("Abcd",1, 3);
        System.out.println(str);

        str.insert(2,"XXX");
        System.out.println(str);
        System.out.println(str.substring(2,5));
        System.out.println(str.delete(2,5));
        System.out.println(str.reverse());



    }

}
