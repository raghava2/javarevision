package com.learning.enumerations;

public enum CoffeeSize {
    BIG, HUGE, OVERWHELMING;
}
