package com.learning.enumerations;

import java.lang.String;


public class EnumInClass {
    enum CoffeeSize {
        SMALL(1), LARGE(2), XL(3);

        private int size;

        CoffeeSize (int size){
        this.size = size;
        }

        public int getSize() {
            return size;
        }
    };

    public CoffeeSize cs;

    public static void main(String[] args) {

        EnumInClass o = new EnumInClass();
        System.out.println(o.makeCoffee(CoffeeSize.XL));
        System.out.println(o.cs.getSize());
    }

    public String makeCoffee(CoffeeSize cs){
        this.cs =cs;
        String result;
        switch (cs){
            case SMALL : {
                result = "S";}

            default : {
                result = "X";
            }
        }
        return result;
    }
}
