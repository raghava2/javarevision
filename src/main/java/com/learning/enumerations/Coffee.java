package com.learning.enumerations;

public class Coffee {

    public static void main(String[] args) {

        Coffee coffee = new Coffee();
        System.out.println(coffee.makecoffee(CoffeeSize.BIG));
    }

    public String makecoffee(CoffeeSize a){
        String result;
        switch(a){

            case BIG : {
                result = "Big one";
            }
            case HUGE : {
                result = "That's huge";
            }
            default :  {
                result = "Is that my coffee?";
            }



//            case BIG : result = "Big one"; break;
//            case HUGE: result = "It's going to be huge";break;
//            case OVERWHELMING: result = "Now that's overwhelming";break;
//            default: result = "Is that my coffee?";
        }

        for(CoffeeSize c : CoffeeSize.values())
            System.out.println(c);

        return result;
    }
}
