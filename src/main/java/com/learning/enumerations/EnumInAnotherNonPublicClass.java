
package com.learning.enumerations;

public class EnumInAnotherNonPublicClass {
    CoffeeSizing.CoffeeSize coffeeSize;

    public static void main(String[] args) {
        EnumInAnotherNonPublicClass n = new EnumInAnotherNonPublicClass();
        n.coffeeSize = CoffeeSizing.CoffeeSize.XL;
    }
}

class CoffeeSizing{
    enum CoffeeSize {SMALL, LARGE, XL };

}
