package com.learning.codility;

import java.util.Arrays;
import java.util.Collections;

public class PassengerCarSolution {
    public static void main(String[] args) {

        int[] P = {1,4,1};
        int[] S = {1,5,1};
        System.out.println(new PassengerCarSolution().solution(P, S));
    }


    public int solution(int[] P, int[] S) {

        Integer[] P1 = new Integer[P.length];
        Integer[] S1 = new Integer[S.length];


        for (int i = 0; i < P.length; i++) {
            P1[i] = P[i];
        }
        for (int i = 0; i < S.length; i++) {
            S1[i] = S[i];
        }

        Arrays.sort(P1,Collections.reverseOrder());
        Arrays.sort(S1, Collections.reverseOrder());


        int totalCars = 0;
        int totalPeople =0;

        for (int peopleinCar:P1) {
            totalPeople = totalPeople+peopleinCar;
        }

            for (int seat : S1) {
                if (totalPeople<=0) break;
                totalPeople = totalPeople - seat;
                totalCars++;
            }



        return totalCars;
    }
}
