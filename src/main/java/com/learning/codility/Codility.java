package com.learning.codility;

import java.util.*;

public class Codility {

    public static void main(String[] args) {
        int [] input={-1,-2,0,1,2,3,5};
        System.out.println(new Codility().solution(input));
    }

    public int solution(int[] A) {
        int result = 1;
        int[] B = A;
        Arrays.sort(B);
        List<Integer> arrayList = new ArrayList<>();
//        arrayList = Arrays.asList(1);


        for (int a: A) {
            if (a>0) {
                Collections.addAll(arrayList, a);
            }
        }

        Set<Integer> set = new HashSet<>(arrayList);


//        arrayList.addAll(B);
        int j = 1;
        for (j =1; j<= A.length;j++) {
            boolean flag = false;
            int searchResult = Arrays.binarySearch(B,j);

            if (searchResult < 0){
                result = j;
                break;
            }

        }

        result =j;
        return result;

    }
}
