package com.learning.codility;

import java.util.*;

public class AccountBalanceCalculator {
    public static void main(String[] args) {

        System.out.println("Hello");

        int[] A = {1, -1, 0, -105, 1};
        String[] D = {"2020-12-31", "2020-04-04", "2020-04-04", "2020-04-14", "2020-07-12" };
        System.out.println(new AccountBalanceCalculator().solution(A, D));
    }

    public int solution(int[] A, String[] D) {

        int balance = 0;
        int fee = 5*12;

        Map<String,Integer> countNegativeTxnForMonth = new HashMap<>();
        for (int i = 0; i < A.length; i++) {
            int count=0;
            String month = D[i].substring(5,7);
            countNegativeTxnForMonth.put(month,count);
            if (A[i]<0){
                countNegativeTxnForMonth.put(month,count+countNegativeTxnForMonth.get(month));

            }
        }

            Map<String,Integer> amountTxnnMonth = new HashMap<>();
        Map<String,Integer> sumMonth = new HashMap<>();
        List<String> monthsList = new ArrayList<>();
        Set<String> monthsSet = new HashSet<>();


        int sum = 0;
        for (int i = 0; i < A.length; i++) {


            String month = D[i].substring(5,7);
            if (!monthsSet.contains(month)){
                sumMonth.put(month,0);
                monthsSet.add(month);
            }
            amountTxnnMonth.put(month,A[i]);

            if(amountTxnnMonth.containsKey(month) && A[i]<0 && sumMonth.get(month) > -100){

                sumMonth.put(month,sumMonth.get(month)+amountTxnnMonth.get(month));

                if (sumMonth.get(month) <= -100 && countNegativeTxnForMonth.get(month)>=3)
                    fee= fee-5;
            }


            balance += A[i];

        }

        balance = balance-fee;

        return balance;



    }

}
