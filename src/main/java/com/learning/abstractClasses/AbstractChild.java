package com.learning.abstractClasses;

public abstract class AbstractChild extends AbstractParent{

    abstract public int method2(); //Abstract child class need not specify method in Abstract parent

    public abstract int x();
}
