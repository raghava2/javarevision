package com.learning.lambdasAndStreams;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LearnStreams {
    public static void main(String[] args) {
//        Stream
        List<String> shops = new ArrayList<>();
        shops.add("Dainty");
        shops.add("Gagan");
        shops.add("Dai");
        shops.add("Dy2");
        shops.add("Dainty3");

        shops.parallelStream().filter(s->s.startsWith("D")).forEach(s -> System.out.print(s + " "));

        System.out.println();
        Function<String, String> f = new Function<String, String>() {
            @Override
            public String apply(String s) {
                return s.toUpperCase();
            }
        };
        shops.stream().map(f).forEach(System.out::println);


        Function<String, String> f2 =  (s) -> s.toLowerCase();

        System.out.println(shops.stream().map(f2).collect(Collectors.toList()));

        Function<Integer, Predicate<String>> functionForStringsLongerThanX = minLength -> stringInput -> stringInput.length()>minLength;

        Predicate<String> stringsLongerThan6 = functionForStringsLongerThanX.apply(6);

        System.out.println("Predicate Function "+stringsLongerThan6);
        System.out.println(shops.stream().filter(functionForStringsLongerThanX.apply(4)).collect(Collectors.toList()));
        System.out.println(shops.stream().filter(stringsLongerThan6).collect(Collectors.toList()));


        List<Integer> intList = Stream.of(1,5,6,3,2,5,8,9,10).collect(Collectors.toList());

        System.out.println(
                intList
                        .stream()
                        .reduce(0, new BinaryOperator<Integer>() {
            @Override
            public Integer apply(Integer integer, Integer integer2) {
                return integer+integer2;
            }
        })
        );

        BinaryOperator<String> binaryOperator = (String x, String y) -> x.concat(y);

        System.out.println(shops.stream().reduce("", binaryOperator).toString());


        intList.parallelStream().filter(number-> number%2==0).reduce(0, (sum,x)-> sum+x);
        System.out.println(intList.parallelStream().filter(number-> number%2==0).collect(Collectors.toCollection(ArrayList::new)).toString());


        Function<String,String> upperCase = str -> str.toUpperCase();
        Function<String,String> reverse = str-> new StringBuilder(str).reverse().toString();

        Function<String,String> uppercasedReversedString = upperCase.andThen(reverse); //this will first uppercase and then reverse
        Function<String,String> reverseduppercasedString = upperCase.compose(reverse); // this will first reverse and then uppercase

        System.out.println(uppercasedReversedString.apply("vahgar"));
        System.out.println(reverseduppercasedString.apply("vahgar"));

        System.out.println(shops.stream().map(uppercasedReversedString).collect(Collectors.toList()));


        //conversions
        Set<Integer> output = Set.of(1,2);

        List<Integer> l23 = output.stream().collect(Collectors.toList());
        System.out.println(l23);
        l23 = new ArrayList<>(output);
        System.out.println(l23);
        HashSet<Integer> x12 =  Arrays.stream(l23.toArray()).mapToInt(value -> (int)value).mapToObj(x2->(Integer)x2).collect(Collectors.toCollection(HashSet::new));

        Arrays.stream(l23.toArray()).collect(Collectors.toCollection(HashSet::new)).stream().forEach(System.out::println);


    }
}
