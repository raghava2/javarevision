package com.learning.lambdasAndStreams;
import java.util.Arrays;
import java.util.Comparator;

public class SecondLargest {
    public static void main(String[] args) {
        int[] array = {9, 5, 2, 7, 1, 8, 6, 3, 4};

        int secondLargest = Arrays.stream(array)

                .boxed()
                .sorted((a,b)->b-a)
                .skip(1) // Skip the smallest element
                .mapToInt(x->x)
                .findFirst().orElseThrow(() -> new IllegalArgumentException("Array should have at least 2 distinct elements."));


        System.out.println("Second largest element: " + secondLargest);
    }
}