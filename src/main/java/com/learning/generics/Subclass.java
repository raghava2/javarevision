package com.learning.generics;

//public class Subclass<T extends Number & Comparable> extends AcceptNumbers<T>{
    public class Subclass extends AcceptNumbers{
    public Subclass(Number operand1, Number operand2) {
        super(operand1, operand2);
    }

    //Even if the subclass don't use Generics any subclass should define a generic T of same type as superclass to support superclass behaviour

    //Generic subclass can extend non generic superclass

    //subclass can declare additional generics parameters E, R K, V etc than those defined in superclass
    public class Subclass2<T extends Number & Comparable, E, R, K, V> extends AcceptNumbers<T>{

        public Subclass2(T operand1, T operand2) {
            super(operand1, operand2);
        }
    }

}
