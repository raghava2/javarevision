package com.learning.generics;

import java.util.*;

public class AcceptNumbers <T extends Number & Comparable> {

    public AcceptNumbers(T operand1, T operand2) {
        this.operand1 = operand1;
        this.operand2 = operand2;
    }

    private T operand1;
    private T operand2;

    public List<T> sortedNumbersList(){
        List<T> output = new ArrayList<>();
        output.add(operand1);
        output.add(operand2);

        Collections.sort(output);
//       output.sort();

       return output;
    }

    public T maxvalue(){
//        Comparable x = new Comparable() {
//            @Override
//            public int compareTo(Object o) {
//                return operand1.getClass();
//            }
//        }
//
//       if( operand1 >  operand2) return operand1;
        T t = operand1.compareTo(operand1) > 0 ? operand1 : operand2;
        return t;
    }

}
