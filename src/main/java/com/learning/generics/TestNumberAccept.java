package com.learning.generics;

import java.util.ArrayList;
import java.util.List;

public class TestNumberAccept {
    public static void main(String[] args) {
        AcceptNumbers<Long> a1 = new AcceptNumbers<>(100L,100L);
        System.out.println(a1.sortedNumbersList());
        System.out.println(a1.maxvalue());

//      List<Number> l = new ArrayList<Double>(); //This is not allowed Generics type should match strictly
        List<Number> l = new ArrayList<>();

        l.add(20.8); //Autoboxing to Number
        l.add (20L);
        l.add(10f);
        l.add(12);
        l.add(0b1);

        l.stream().forEach(System.out::println);

    }
}
