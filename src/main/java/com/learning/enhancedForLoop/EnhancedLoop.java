package com.learning.enhancedForLoop;

import java.io.IOException;

public class EnhancedLoop {

    public static void main(String[] args) throws IOException {

        int[] x = {1,2,3,4,5,6,7,8,8,9};
        int y =0;
//        for(y:x) //a new block variable must always be declared in enhanced for loop
        for (int y1:x) {


        }

        switch (""+""){

        }
        System.out.println('j'+'a'+'v'+'a'); //this will print ascii sum

    }
}
