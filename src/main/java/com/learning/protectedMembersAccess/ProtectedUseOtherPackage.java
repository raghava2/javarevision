package com.learning.protectedMembersAccess;

import com.learning.javaInheritanceGarbageCollection.Parent;

public class ProtectedUseOtherPackage {
        public static void main(String[] args) {
            Parent c = new Parent();
//            System.out.println(c.def);
//            System.out.println(c.pro);
            ChildInOtherPackage i = new ChildInOtherPackage();

//            i.pro;
        }

    }
