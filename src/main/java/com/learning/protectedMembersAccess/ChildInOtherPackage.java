package com.learning.protectedMembersAccess;

import com.learning.javaInheritanceGarbageCollection.Parent;

public class ChildInOtherPackage extends Parent {
    public static void main(String[] args) {
        Parent c = new Parent();
//        System.out.println(c.def);
//        System.out.println(c.pro);
    }

    public void getProtectedField(){
        System.out.println(pro);
//        System.out.println(def);

    }
}
