package com.learning.exceptionHandling;

class UserDefinedException extends java.lang.Exception{

public UserDefinedException(String exceptionMessage){


    super(exceptionMessage);
        }

}
public class Exception {
    public static void main(String[] args) {

        Exception ex = new Exception();
        try {
            ex.reverse("r");
        } catch (UserDefinedException e) {
//            System.out.println(e.getMessage());
//            System.out.println(e.getCause());
//            System.out.println(e.getStackTrace());
//            System.out.println(e.toString());
//            System.out.println(e.fillInStackTrace());
//            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
            System.out.println(e.toString());

//            e.notifyAll();
            throw new RuntimeException(e);

        }
        catch (RuntimeException e){

            e.printStackTrace();
        }
        finally {
            System.out.println("main has finished");
        }

    }


    String reverse(String str) throws UserDefinedException{

        if (str.length()==1){
            System.out.println("inside if catching exception next");
            throw new UserDefinedException(str);
        }

        String result = "";
        StringBuilder str2 = new StringBuilder(str);
        str2.reverse();
        result = str2.toString();
        return result;
    }

}
