package com.learning.exceptionHandling;

public class C1 extends P1 {
    @Override
    void d() // child can throw no exception or a child exception of one declared in parent
    {}

}
