package com.learning.collections;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class StreamsUse {

    public static void main(String[] args) {
        List<String> countryList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            if (i % 2 == 0) {
                countryList.add("country" + i);

            } else {
                countryList.add("cou" + i);

            }
        }


        countryList.stream().filter(new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.length() > 4;
            }
        }).forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        });

        countryList.stream().filter(new Predicate<String>() {
            @Override
            public boolean test(String s) {
//                System.out.println(s + "" + (Integer.parseInt(s.substring(s.length()-1))));
                return (Integer.parseInt(s.substring(s.length() - 1))) >= 4;
            }
        }).forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        });

        countryList.stream().filter(s -> {
//                System.out.println(s + "" + (Integer.parseInt(s.substring(s.length()-1))));
                    return (Integer.parseInt(s.substring(s.length() - 1))) <= 4;
                }
        ).forEach(s -> {
            System.out.println(s);
        });


        for (String d: countryList
             ) {
            System.out.println("here is our d " + d);
        }


        countryList.add("");
        countryList.stream().filter(s-> s.length() >4
//                System.out.println(s + "" + (Integer.parseInt(s.substring(s.length()-1))));
//                    return (Integer.parseInt(s.substring(s.length() - 1))) <= 4;
//                }
        ).forEach(s -> {
            System.out.println(s);
        });
    }
}

