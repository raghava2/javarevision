package com.learning.collections;

import java.util.*;
import java.util.stream.Collectors;

public class Others {

    public static void main(String[] args) {

        String[] inputStrings = {"raGHav", "aGGarWaL", "kEShav123"};

        Queue<String> q = new PriorityQueue<>(List.of(inputStrings));
//        q.add("raghav");
//        q.add("kEShav123");
//        q.add("kEShav123");

        Queue<String> q1 = new PriorityQueue<>();

        System.out.println("Add in queue using stream");
        Arrays.stream(inputStrings).forEach(s-> {System.out.println(s); q1.add(s);});
        System.out.println("Addition done, now apply operation");


        q1.parallelStream()
                .map(s -> {return s.substring(0,1).toUpperCase() + s.substring(1).toLowerCase();})
                .filter(s-> s.charAt(0)>= 'A' && s.charAt(0)<='z')
                .forEach(s->System.out.print(s+ " "));

        System.out.println(
                """
                        add using 
                        for loop
                        """);

        for (String x:inputStrings) {
            q1.add(x);

        }




        Queue<String> qr =q.stream()
                .map(s -> {return s.substring(0,1).toUpperCase() + s.substring(1).toLowerCase();})
                .filter(s-> s.charAt(0)>= 'A' && s.charAt(0)<='z')
//                .forEach(s->System.out.print(s+ " "));
                .collect(Collectors.toCollection(PriorityQueue::new));
//
//
//        .forEach(s-> System.out.println(s.length()));

        qr.forEach(System.out::println);
        System.out.println(qr.peek());



    }
}
