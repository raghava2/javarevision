package com.learning.constructor;

public class FooChild extends Foo{
    private static int x;
    private int u;

    FooChild() {
        super(x); //only static variables can be called from arguments of super() and this() constructor calls
    }

    FooChild(int x){
        this();
    }

    FooChild(int x, int y){
//        this(u);
        this(x);
    }

}