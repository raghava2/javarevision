package com.learning.literals;

public class FloatingLiterals {
    public static void main(String[] args) {
//        float f = 12.4; by default decimal points are stored and treated as double for float add f
        float f = 12.4f;
        double d =12.4;
    }
}
