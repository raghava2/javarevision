package com.learning.literals;

public class IntegerUnderscoreLiterals {

    private final int MILLION = 1_000_000;
    private final int binary = 0B01_000_000;
    private final int octal = 01_000_000;
    private final int hex = 0x01_0abcdef;



    private final int hexadecimal = 0B01_000_000;




    public static void main(String[] args) {
        IntegerUnderscoreLiterals ob = new IntegerUnderscoreLiterals();


        int x1M1 = ob.MILLION+1;

        System.out.println(x1M1);
        System.out.println(ob.binary);
        System.out.println(ob.octal);
        System.out.println(ob.hex);
        System.out.println(ob.hexadecimal);


    }
}
