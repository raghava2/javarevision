package com.learning.literals;

public class CharacterLiterals {

    public static void main(String[] args) {
        char a =1;
        char b ='@';

        char c = '\u0000';
        char d = 65535;
        char d1 = (char)65536;

        //char range is 0 to 65535
        System.out.println(a==d1);


        char newL = '\n';
        char doubleQ = '\"';

        System.out.println((int)a+"\n"+(int)b+"\n this"+c+"\n"+d1+"\n"+a+"\n"+newL+"\n"+doubleQ);

    }

}
