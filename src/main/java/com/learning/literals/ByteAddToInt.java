package com.learning.literals;

public class ByteAddToInt {
    public static void main(String[] args) {
        byte a = 2;
        byte b = 2;
        byte c = 0;
//        byte c = a+b;  //This won't compile because anything smaller than int adds to a 32 bit int;
        // int +byte -> int
        //short +short -> int
        short a1 =1;
        short b1 =1;

//        short c1 =a1+b1; // won't compile same reason

        int x = 65536;
        int x2 = 65536;
        int x3 = 65536;

        long y = (long)x*x2*x3; //explicit cast is required because result will be int
        // as the result is int and out of range of int, so we get a 0
        System.out.println(y);
        long r = 1231234243;
        long s = 1242344379;

        long res = r*s*r*s*r*s*r*s*r*s*r*s*r*s*r*s*r*s*r*r*s*r*s*r*s*r*s*r*s;
        System.out.println(res);
        ByteAddToInt s5 = new ByteAddToInt();
//        System.out.println(s5 instanceof GarbageCollectionRuntimeThread);
// instanceof operator checks only in related heirarchies '././.fd.,.fma,.nmfbnbnsm,.?
//        'Q;WLSKJHGHFJSKDAL;1234567890qwertyuiopasdfghjklzxcvbnm,./\]=-
// if objects and classes being checked are not related then it throws an error

        boolean ch3,ch4, ch5;

        ch4 = ch3=true;
        System.out.println(ch3+" "+ch4);

        ch5 = ch3 = false;
        System.out.println(ch3+"\uD83D\uDE03 "+ch5);

    }
}
