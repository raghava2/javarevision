package com.learning.varargs;

public class Varargs {

    public int acceptAsManyIntegers(int ... s)
    {
        return 0;
    }
    public int acceptAsManyIntegers(int t, int ... s)
    {
        int result=0;

        for (int i:s
             ) {
            result += i;
        }
        return result;
    }
    public int acceptAsManyIntegers1(int r, int s, int ...f) // last parameter only vararg
    {
        return r+s;
    }

    public static void main(String[] args) {
//        System.out.println(new Varargs().acceptAsManyIntegers(1,2,3,4,5,6,7,8,9,10)); Ambiguous method calls

    }
}
