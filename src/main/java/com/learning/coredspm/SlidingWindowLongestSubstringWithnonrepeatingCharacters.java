package com.learning.coredspm;

import java.util.HashMap;
import java.util.Map;

public class SlidingWindowLongestSubstringWithnonrepeatingCharacters {

    public static void main(String[] args) {
        String input = "aababcdefabcdefgbebe";
        String out = findLongestSubstringWithNonRepeatingCharacters(input);
        System.out.println(out);
    }

    private static String findLongestSubstringWithNonRepeatingCharacters(String input) {
        String output = "";
        int length = input.length();
        int start = 0, end = 0;
        Map<Character, Integer> mapCharacterCount = new HashMap<>();

        while (end < length) {
            char currentChar = input.charAt(end);
            mapCharacterCount.put(currentChar, mapCharacterCount.getOrDefault(currentChar, 0) + 1);
            int windowSize = (end - start) + 1;

            if (mapCharacterCount.size() == windowSize && mapCharacterCount.size() >= output.length()) {
                output = input.substring(start, end + 1);
            } else if (mapCharacterCount.size() < windowSize) {

                mapCharacterCount.put(input.charAt(start), mapCharacterCount.get(input.charAt(start)) - 1);

                if (mapCharacterCount.get(input.charAt(start)) == 0) {
                    mapCharacterCount.remove(input.charAt(start));
                }
                start++;
            }
//            else if (mapCharacterCount.size()>windowSize) {
//                System.out.println("This condition should never be reached");
//
//            }


            end++;
        }

        return output;
    }

}
