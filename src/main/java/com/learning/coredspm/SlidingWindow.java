package com.learning.coredspm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* find the longest substring where there are 'k' unique characters
* */
public class SlidingWindow {
    public static void main(String[] args) {
        String input = "aabacbebebe";
        int k = 3;

        String out = findLongestSubstringWithKUniqueCharsOptimized(input, k);
        System.out.println(out);


    }

    private static String findLongestSubstringWithKUniqueChars(String input, int k) {
        String output ="";
        int maxLen = 0;

        boolean countUnique = countUniqueCharacters(input, k);

        for (int i = 0; i < input.length(); i++) {
            for (int j = i; j <= input.length(); j++) {
                if(countUniqueCharacters(input.substring(i,j),k) && input.substring(i,j).length()>maxLen){
                    output = input.substring(i,j);
                    maxLen = input.substring(i,j).length();
                }
            }
        }

        return output;
    }

    private static boolean countUniqueCharacters(String input, int k) {
        char[] c = input.toCharArray();
        Set<Character> set = new HashSet<>();

        for (char ch:c) {
            Character c1 = ch;
            set.add(c1);
        }
        return set.size() ==k;
    }

    private static String findLongestSubstringWithKUniqueCharsOptimized(String input, int k) {
        String out ="";
        int max = 0;
        Map<Character,Integer> map = new HashMap<>();
        int end=0,start=0;
        while (end<input.length()) {

            int fr = map.getOrDefault(input.charAt(end), 0);
            map.put(input.charAt(end), fr + 1);

            if (map.size() == k) {
                int presentSize = end - start + 1;
                out = input.substring(start, end + 1);
                max = Math.max(max, presentSize);

            } else if (map.size() > k) {
                start++;
                map.put(input.charAt(start), map.get(input.charAt(start)) - 1);
                if (map.get(input.charAt(start)) == 0) {
                    map.remove(input.charAt(start));
                }
            }
            end++;
        }
        return out;
    }


}
