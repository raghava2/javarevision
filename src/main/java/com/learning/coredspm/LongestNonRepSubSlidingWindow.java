package com.learning.coredspm;

import java.util.*;

public class LongestNonRepSubSlidingWindow {
    public static void main(String[] args) {
        String input = "abcdeabcdefabcabcdefgh";
        String out = findLongestStringWithoutRepititionSlideWin(input);
        System.out.println(out);

    }

    private static String findLongestStringWithoutRepititionSlideWin(String input) {
        String out = "";
        int j = 0,i=0;
        int max =0;
        while ( i<=j && j<input.length()){
            String current = input.substring(i,j+1);
            if (hasUniqueChars(current)){


                if (current.length()>max){
                out = current;
                max = current.length();}
                j++;

            }
            else {
                i++;
            }

        }

        return out;
    }

    private static boolean hasUniqueChars(String substring) {
        char[] chars = substring.toCharArray();
        List<Character> list = new ArrayList<>();

        for (char c: chars) {
            if (!list.contains(c)){
            list.add(c);}
            else {
                return false;
            }
        }
        return true;
    }


}
