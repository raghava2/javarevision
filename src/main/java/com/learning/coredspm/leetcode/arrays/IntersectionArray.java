package com.learning.coredspm.leetcode.arrays;

import java.util.*;
import java.util.stream.Collectors;

public class IntersectionArray {


    public static void main(String[] args) {
       int[] nums1 = {1,2,2,1};
       int[] nums2 = {2,2};
        Arrays.stream(new IntersectionArray().intersection(nums1, nums2)).forEach(System.out::println);


    }
    public int[] intersection(int[] nums1, int[] nums2) {
//        int[] output = new int[0];

        Set<Integer> output = new HashSet<>();
        List<Integer> a = Arrays.stream(nums2).boxed().collect(Collectors.toList());

        for (int n:nums1) {

            if (a.contains(n))output.add(n);
        }

       return output.stream().mapToInt(x->x).toArray();

    }
}
