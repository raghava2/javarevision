package com.learning.coredspm.leetcode.arrays;

public class SearchInsertPosition {

    public static void main(String[] args) {
        int[] nums = {1,3,5,6};
        System.out.println(new SearchInsertPosition().searchInsert(nums, 5));
        System.out.println(new SearchInsertPosition().searchInsert(nums, 7));
        System.out.println(new SearchInsertPosition().searchInsert(nums, 2));
    }
    public int searchInsert(int[] nums, int target) {
        int end = nums.length-1, start = 0,mid;

        while(start<=end){
            mid = (end+start)/2;

            if(target == nums[mid]){
                return mid;
            }
            if (start==end){
                return start+1;
            }

            if(target>nums[mid]){
                if(target<nums[mid+1]){
                    return mid;
                }
                start = mid+1;
            }
            else if(target<nums[mid]){
                if(target>nums[mid-1]){
                    return mid;
                }
                end = mid-1;
            }
        }
        return -1;
    }
}
