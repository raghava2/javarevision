package com.learning.coredspm.leetcode.arrays;

import java.util.Arrays;

public class MaxProfit {
    public static void main(String[] args) {

        int[] in ={7,1,5,3,6,4};
        System.out.println(new MaxProfit().maxProfit(in));
    }

    public int maxProfit(int[] prices) {

        int maxAftMin=0;
        int min = Arrays.stream(prices).min().getAsInt();
        int indexMin = 0;

        for (int i = 0; i < prices.length; i++) {
            if (prices[i]==min){
                indexMin=i;
                break;
            }
        }


        int[] newArr = Arrays.copyOfRange(prices,indexMin, prices.length);
        if (newArr.length>1){
            maxAftMin = Arrays.stream(newArr).max().getAsInt();
        }
        else return 0;
        return maxAftMin-min;


    }
}
