package com.learning.coredspm.leetcode.arrays;

public class MaximumProductSubarray {

    public static void main(String[] args) {

        int[] in = {2,3,-2,4};

        int out = new MaximumProductSubarray().maxProduct(in);
        System.out.println(out);
    }

    public int maxProduct(int[] nums) {
        if (nums.length<2) return nums[0];
        int maxProduct = nums[0]*nums[1];
        int start=0, end=1;


        while (end<nums.length) {
            if (
            nums[start]*nums[end] >maxProduct){
                maxProduct = nums[start]*nums[end];
            }
            start++;end++;

        }

        return maxProduct;
    }

}
