package com.learning.coredspm.leetcode.arrays;

import java.util.Arrays;

public class MergeSortedArrays {
    public void merge(int[] nums1, int m, int[] nums2, int n) {

        int i=0;
        for (int k = m; k < nums1.length; k++) {
            nums1[k]=nums2[i];
            i++;
        }
        Arrays.sort(nums1);
    }
}
