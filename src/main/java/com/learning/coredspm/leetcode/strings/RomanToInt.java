package com.learning.coredspm.leetcode.strings;

public class RomanToInt {

    public static void main(String[] args) {
        System.out.println(new RomanToInt().romanToInt1( "IX"));
    }

    public int romanToInt(String s) {
        char[] chars = s.toCharArray();
        int number = 0;

        char prev = chars[0];
        for (int i = 0; i < chars.length; i++) {

            switch (chars[i]){
                case 'M': {number+=1000;
                    if (prev=='C') {
                        number -= 200;
                    }
                        break;}
                case 'D': {number+=500;
                    if (prev=='C') {
                        number -= 200;
                    }
                    break;}
                case 'C': {number+=100;
                    if (prev=='X') {
                        number -= 20;
                    }
                    break;
                }
                case 'L': {number+=50;
                    if (prev=='X') {
                        number -= 20;
                    }
                    break;}
                case 'X': {number+=10;
                    if (prev=='I') {
                        number -= 2;
                    }
                    break;}
                case 'V': {number+=5;
                    if (prev=='I') {
                        number -= 2;
                    }
                    break;}
                case 'I': {number+=1;break;}

                }

            prev=chars[i];

        }


            return number;
        }


    public int romanToInt1(String s) {
        int ans = 0, num = 0;
        for (int i = s.length()-1; i >= 0; i--) {
            switch(s.charAt(i)) {
                case 'I': num = 1; break;
                case 'V': num = 5; break;
                case 'X': num = 10; break;
                case 'L': num = 50; break;
                case 'C': num = 100; break;
                case 'D': num = 500; break;
                case 'M': num = 1000; break;
            }
            if (4 * num < ans) ans -= num;
            else ans += num;
        }
        return ans;
    }

}
