package com.learning.coredspm.leetcode.strings;

import java.util.List;
import java.util.stream.Collectors;

public class ReverseVowels {
    public static void main(String[] args) {

        String s = "hello";
        System.out.println(new ReverseVowels().reverseVowels(s));
    }

    public String reverseVowels(String s) {

        List<Character> listVowels = s.chars().mapToObj(x -> (char) x).filter(x -> isVowel(x)).collect(Collectors.toList());

        int j = 0;
        char[] chars = s.toCharArray();
        for (int i = chars.length-1; i >= 0; i--) {
            if (isVowel(chars[i]))
            {chars[i]=listVowels.get(j);
            j++;}

        }
        return String.valueOf(chars);

    }

    public boolean isVowel(char x) {
        return (x == 'a' || x == 'e' || x == 'i' || x == 'o' || x == 'u' || x == 'A' || x == 'E' || x == 'I' || x == 'O' || x == 'U');
    }

}
