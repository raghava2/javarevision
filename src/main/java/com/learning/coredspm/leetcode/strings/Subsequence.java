package com.learning.coredspm.leetcode.strings;


public class Subsequence {

//    @Test
    public static void main(String[] args) {
//        Assert.assertTrue(new Subsequence().isSubsequence("", "ahbgdc"));
        System.out.println(new Subsequence().isSubsequence("ahbgdc", "ahbgdc"));
    }
    public boolean isSubsequence(String s, String t) {

        if (s.length()<1) return true;
        if (s.length()>t.length()) return false;

        int i=0;

        for (int k = 0; k < t.length(); k++) {
            if (t.charAt(k) == s.charAt(i)){
                i++;
            }
        }


        return i == s.length();
    }
}
