package com.learning.coredspm.leetcode.strings;

import java.util.*;

import static java.util.Map.Entry;
import java.util.stream.*;

public class Isomorphic {

    public static void main(String[] args) {
        String s = "ab", t = "ca";

        System.out.println(new Isomorphic().isIsomorphic4(s,t));
    }

    public boolean isIsomorphic(String str1, String str2){
        boolean out = false;
        ArrayList<Character> setChars =str1.chars().mapToObj(x->(char)x).collect(Collectors.toCollection(ArrayList::new));

        for (char c: setChars
             ) {
            int index = str1.indexOf(c);
            String charToReplace = String.valueOf(str2.charAt(index));

            str2 = str2.replaceAll(charToReplace, String.valueOf((char) c));

            str2.replace(str2.charAt(index), c);
        }

        out = str2.equals(str1);

        return out;

    }

    public boolean isIsomorphic2(String str1, String str2){
        boolean out = false;

        if (str1.length() != str2.length()) return false;
        Map<Character,Integer> charCountMap = new LinkedHashMap<>();
        Map<Character,Integer> charCountMap2 = new LinkedHashMap<>();

        char[] chars = str1.toCharArray();

        for (char ch: chars) {
            charCountMap.put(ch, charCountMap.getOrDefault(ch,0)+1);
        }

        chars = str2.toCharArray();

        for (char ch: chars) {
            charCountMap2.put(ch, charCountMap2.getOrDefault(ch,0)+1);
        }

        Iterator<Entry<Character,Integer>> iterator = charCountMap.entrySet().iterator();
        Iterator<Entry<Character,Integer>> iterator2 = charCountMap2.entrySet().iterator();

        while (iterator.hasNext() && iterator2.hasNext())
        {

            Entry<Character,Integer> e1 = iterator.next();
            Entry<Character,Integer> e2 = iterator2.next();

            if(!(e1.getValue() == e2.getValue())) {
                return false;
            }
                iterator.remove();
                iterator2.remove();
        }

        return charCountMap2.isEmpty();
    }


    public boolean isIsomorphic3(String str1, String str2){

        int start=0,end=0;

        while (end<str1.length())
        {
            char curr = str1.charAt(end);

            if (str1.charAt(start)==curr){
                end++;
            }

        }

        return true;
    }

    public boolean isIsomorphic4(String s, String t) {
        Map<Character, Integer> map1 = new HashMap<>();
        Map<Character, Integer> map2 = new HashMap<>();

        for(Integer i = 0; i <s.length(); i ++) {
            if(map1.put(s.charAt(i), i) != map2.put(t.charAt(i), i))
                return false;
        }
        return true;
    }
}
