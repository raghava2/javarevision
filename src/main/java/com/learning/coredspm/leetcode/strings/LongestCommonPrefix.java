package com.learning.coredspm.leetcode.strings;

public class LongestCommonPrefix {

    public static void main(String[] args) {
        String[] strs = {"flower","flower","flower","flower"};
        System.out.println(new LongestCommonPrefix().longestCommonPrefix(strs));
    }

    public String longestCommonPrefix(String[] strs) {

        String commonpf = "";

        if (strs.length==1){
            return strs[0];
        }
        int j=1;
        while (j<=strs[0].length()) {
            commonpf = strs[0].substring(0, j);
            for (String str : strs) {

                if (!str.startsWith(commonpf)) {
                    return commonpf.substring(0,commonpf.length()-1);
                }
            }
            j++;
        }
        return commonpf;
    }
}
