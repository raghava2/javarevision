package com.learning.coredspm.leetcode.strings;

public class PalindromeSentence {

    public static void main(String[] args) {
        String s = "0P";
        System.out.println(new PalindromeSentence().isPalindrome(s));
    }

    public boolean isPalindrome(String s) {

        char[] chars = s.toLowerCase().toCharArray();

        String stripedOfNonAlpha = "";
        for (char ch:chars) {
            if ((ch>='a' && ch<='z') || (ch>= '0' && ch<= '9')){
                if (Character.isAlphabetic(ch))
                stripedOfNonAlpha = stripedOfNonAlpha+ch;
            }
        }

        return isPalindromeCheck(stripedOfNonAlpha);

    }

    private boolean isPalindromeCheck(String str){
        StringBuilder rev = new StringBuilder(str);

        if (str.equals(rev.reverse().toString())) return true;

        return false;
    }
}
