package com.learning.coredspm.leetcode.strings;

import java.util.*;
import java.util.stream.Collectors;

public class Anagram {

    public static void main(String[] args) {

        String s = "anagram1", t = "nagar1am";
        System.out.println(new Anagram().isAnagram(s,t));
    }

    public boolean isAnagram(String str1, String str2){
        boolean output=false;
        if (str1.length()!=str2.length()) return false;
        char[] chars = str1.toCharArray();
        List<Character> l1 =str1.chars().mapToObj(c->(char)c).collect(Collectors.toList());

        char[] chars2 = str2.toCharArray();
        for (char c :chars2) {
            if(!l1.remove((Character) c)){
                return false;
            }
        }
        output = l1.isEmpty();


        return output;
    }
}
