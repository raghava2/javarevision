package com.learning.coredspm;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LongestNonRepetitiveSubstring {

    public static void main(String[] args) {
        String input = "abcdeabcdefabcabcdefgh";
        String out = findLongestStringWithoutRepitition1(input);
        System.out.println(out);

    }

    private static String findLongestStringWithoutRepitition(String input) {
        String out = "";
        char[] chars = input.toCharArray();
        Character[] characterArray = input.chars().mapToObj(c -> (char) c).toArray(Character[]::new);

        Set<Character> set = new HashSet<>(List.of(characterArray));

        Map<Character, int[]> mapCharacterIndex = new HashMap<>();

        mapCharacterIndex.putAll((Map<? extends Character, ? extends int[]>) set);

        for (int i = 0; i < input.length(); i++)

            if (set.contains(input.charAt(i))) {
                input = input.substring(i);
//            }
//
//        }
//
//
////            input = input.su
////        }
////        ArrayUtils.to
//
//
////        Character[] characterArray = input.chars().mapToObj(c -> (char) c).toArray(Character[]::new);
//        Arrays.stream(characterArray)
//                .forEach(c -> {
//                    System.out.println(input.substring(input.indexOf(c)));});

//        Stream<Character> stream = Arrays.s
//        for (char c: chars) {
//            set.add(c);Î
//        }



            }
        return out;
    }

    private static String findLongestStringWithoutRepitition1(String input) {
        String o = "";
        char[] chars = input.toCharArray();
        Map<Character, int[]> charIndices = new HashMap<>();




            int k =-1;
            for (int i = 0; i < input.length(); i++) {

                int[] a = new int[input.length()];
                if (charIndices.containsKey(input.charAt(i))){
                    a = charIndices.get(input.charAt(i));

                    int nextIndex =0;
                    for (int j = 0; j < a.length; j++) {
                        if (a[j]==0){
                            nextIndex = j;
                            break;
                        }

                    }
                    a[nextIndex] = i+1;
                    charIndices.put(input.charAt(i), a);
                }
                else {

                    a[0] = i+1;
                    charIndices.put(input.charAt(i), a);
                }
            }

            int max = 0;
            int start =0;
            int end =0;
        for (int[] ar: charIndices.values()) {
            for (int i = 0; i < ar.length-1; i++) {

                if(ar[i+1]-ar[i] > max) {
                    end = ar[i + 1];
                    start = ar[i];
                    max = Math.max(ar[i + 1] - ar[i], max);

                }
                if (ar[i+1]==0){
//                    max = Math.max(input.length() - ar[i], max);
                    break;
                }

                }
            }


        System.out.println(max);
        return input.substring(start-1, end-1);
    }

}
