package com.learning.anshul;


import java.time.LocalDate;

class Customer{

    enum Services {

        WASHING(100), YEARLY_SERVICE(1000), DAMAGE_FIX(5000);
        public final int cost;
        private Services(int cost){
            this.cost =cost;
        }
    };

    public Customer(String name, int car_model, LocalDate date_of_last_visit, Services[] availedServiceOnLastVisit) {
        this.name = name;
        this.car_model = car_model;
        this.date_of_last_visit = date_of_last_visit;
        this.availedServiceOnLastVisit = availedServiceOnLastVisit;
    }

    String name;
    int car_model;
    LocalDate date_of_last_visit;
    Services[] availedServiceOnLastVisit;

    public boolean isGivenServiceAvailed(Services serviceX){

        for (Services service: this.availedServiceOnLastVisit) {
            if (service.equals(serviceX)){
                return true;
            }
        }
        return false;
    }
}