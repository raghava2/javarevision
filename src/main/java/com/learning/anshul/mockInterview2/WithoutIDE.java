package com.learning.anshul.mockInterview2;
// Online Java Compiler
// Use this editor to write, compile and run your Java code online
import java.util.*;

public class WithoutIDE {
    public static void main(String[] args) {
        int [] inputArray1 = {1,2,4,5}, inputArray2 = {6,2,4,15};

        int[] out = findUnionWithoutRepetition(inputArray1, inputArray2);

        Arrays.stream(out).forEach(System.out::println);
//        for(int i: out){
//            System.out.println(i);
//        }
    }

    public static int[] findUnionWithoutRepetition(int [] inputArray1, int[] inputArray2){
        Set<Integer> set = new HashSet<>();
        for(int i: inputArray1){
            set.add(i);
        }

        for(int i: inputArray2){
            set.add(i);
        }

        return set.parallelStream().mapToInt(x->x).toArray();

//        answer = set.parallelStream().mapToInt(x->x).toArray();

//        int[] answer = new int[set.size()];
//        int j=0;
//
//        for(Integer i: set){
//            answer[j]=i;
//            j++;
//        }

//        return answer;
    }
}