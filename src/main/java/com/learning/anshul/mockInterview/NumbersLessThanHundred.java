package com.learning.anshul.mockInterview;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class NumbersLessThanHundred {
    public static void main(String[] args) {
        List<Integer> input = List.of(10,15,100,102,105,9,67);

        ArrayList<Integer> inputArrayList = new ArrayList<>(input);

        ArrayList<Integer> out =new NumbersLessThanHundred().calcNumbersLessThan100(inputArrayList);

        out.stream().forEach(System.out::println);
    }

    public ArrayList<Integer> calcNumbersLessThan100(ArrayList<Integer> inputArray){
        List<Integer> output = inputArray.stream().filter(x -> x <100).collect(Collectors.toList());

        ArrayList<Integer> out = new ArrayList<>(output);

        return out;
    }

}
