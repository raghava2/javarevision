package com.learning.anshul.mockInterview;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Trains {

    /*

There’s a railway station with trains arriving and departing. You are given a list of arrival times and departure times in a 24 hour format. Write a program to find the minimum number of platforms needed to cater to the rail traffic on a given day. Make following assumptions:
List of arrival and departure times are always sorted
Number of arrival is always equal to number of departures
Order of arrival is the same as order of departure - train that arrives first, departs first
1 train would need 1 platform while it is at the station
Example : arrival_list         = {1300, 1400, 1530, 1700}
	     deparature_list = {1330, 1410, 1800, 1830}
Answer  : 2 platform


*/


//    calc the time for each train
//    map platform -> "P1", flag boolean

    public static void main(String[] args) {


//        int[] arrival_list    = {1300, 1400, 1530, 1700, 1725, 1730, 1740};
//        int[] deparature_list = {1330, 1410, 1741, 1750, 1800, 1830, 1900};
//
//        System.out.println(new Trains().calculateNumberOfPlatforms(arrival_list, deparature_list));


        int[] arrival_list1    = {1300, 1750, 1745, 1400, 1530, 1700, 1725, 1730 };
        int[] deparature_list1 = {1330, 1755, 1750, 1410, 1800, 1830, 1900, 1740 };

        System.out.println(new Trains().countComplexPlatforms(arrival_list1, deparature_list1));
    }


    public int calculateNumberOfPlatforms(int[] arrival_list, int[] deparature_list){
        int platformCount=1;
        int lastDepartingTrainTime = deparature_list[0];


        int j=0;
        for (int i = 0;  i < arrival_list.length-1; i++) {

            if (arrival_list[i+1] > lastDepartingTrainTime){

                lastDepartingTrainTime = deparature_list[j+1];
                j++;

            }
            else {

                platformCount++;
            }
        }

        platformCount = deparature_list.length - j;
        return platformCount;

    }

    public int countComplexPlatforms(int[] arrival_list, int[] deparature_list){

        List<int[]> sortedList = sortArrivalAndAdjustDepartureList(arrival_list, deparature_list);

        arrival_list = sortedList.get(0);
        deparature_list = sortedList.get(1);


        List<Platform> platformList = new ArrayList<>();
//        platformList.add(new Platform(0, deparature_list[0]));

        for (int i = 0;  i < arrival_list.length-1; i++) {
            platformList.add(new Platform(i, deparature_list[i]));

            Iterator<Platform> iterator = platformList.iterator();

            while (iterator.hasNext()) {
                Platform platform = iterator.next();
                if (platform.canBeVacatedAtTime(arrival_list[i + 1])) {
                    System.out.println("Platform "+ (platform.getId()+1) + ": is vacated");
                    iterator.remove();
                }
            }
        }
            return platformList.size()+1;
    }

    private List<int[]> sortArrivalAndAdjustDepartureList(int[] al, int[] dp) {

        int[] arrivalList = Arrays.copyOf(al, al.length);
        int[] deparatureList = Arrays.copyOf(dp, dp.length);

        for (int j = 0; j < arrivalList.length-1; j++) {
            for (int i = 0; i < arrivalList.length-1; i++) {

                if (arrivalList[i]>arrivalList[i+1]){
                int temp = arrivalList[i+1];
                arrivalList[i+1] = arrivalList[i];
                arrivalList[i] = temp;

                int temp1 = deparatureList[i+1];
                deparatureList[i+1] = deparatureList[i];
                deparatureList[i] = temp1;
                }
            }


        }

        List<int[]> lists = new ArrayList<>();
        lists.add(arrivalList);
        lists.add(deparatureList);
        return lists;

    }


}

class Platform {



    private int id;
    private int departureTimeOfStandingTrain;

    protected Platform(int id, int departureTimeOfStandingTrain){
        this.id = id;
        this.departureTimeOfStandingTrain = departureTimeOfStandingTrain;
    }

    protected boolean canBeVacatedAtTime(int time){
        boolean out =false;
        if (departureTimeOfStandingTrain < time){
            out = true;
        }
        return out;
    }
    public int getId() {
        return id;
    }

}