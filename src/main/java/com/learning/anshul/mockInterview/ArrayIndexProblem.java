package com.learning.anshul.mockInterview;


import java.util.Arrays;

/**
 * Given an input integer array of length n that can hold values between 0 to 9,
 * write a program that generates an output array of the same length where the value at each index
 * of the output array is equal to the number of times this index occurred as a value in the input array.
 * For example: If Input array is {0,1,1,1,3,0}, then the output array should be {2, 3, 0,1,0,0}
 *
 * */

public class ArrayIndexProblem {

    public static void main(String[] args) {
        int[] input = {0,1,1,1,3,0};
        int out[] = new ArrayIndexProblem().calculateNumberOfTimesIndexAppears(input);
//        System.out.println(Arrays.toString(out));
        Arrays.stream(out).forEach( x -> System.out.print(x+" "));
    }

    private int[] calculateNumberOfTimesIndexAppears(int[] input) {
        int[] output = new int[Math.min(input.length, 10)];

        for (int i = 0; i < Math.min(input.length, 10); i++) {
            final int j=i;
            int out = (int) Arrays.stream(input).filter(x -> x==j).count();
           output[i]= out;
        }

        return output;
    }

}
