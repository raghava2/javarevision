package test.java.com.learning.coredspm.leetcode.strings;

import com.learning.coredspm.leetcode.strings.Subsequence;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SubsequenceTest {

    @Test
    @DisplayName("Should return true when the first string is empty")
    void isSubsequenceWhenFirstStringIsEmpty() {
        Subsequence subsequence = new Subsequence();
        boolean result = subsequence.isSubsequence("", "ahbgdc");
        assertTrue(result);
    }

    @Test
    @DisplayName("Should return false when the first string is longer than the second string")
    void isSubsequenceWhenFirstStringIsLongerThanSecondString() {
        Subsequence subsequence = new Subsequence();
        boolean result = subsequence.isSubsequence("abcde", "abcd");
        assertFalse(result);
    }

    @Test
    @DisplayName("Should return false when the first string is not a subsequence of the second string")
    void isSubsequenceWhenFirstStringIsNotSubsequenceOfSecondString() {
        Subsequence subsequence = new Subsequence();
        String s = "abx";
        String t = "ahbgdc";

        assertFalse(subsequence.isSubsequence(s, t));
    }

    @Test
    @DisplayName("Should return true when the first string is a subsequence of the second string")
    void isSubsequenceWhenFirstStringIsSubsequenceOfSecondString() {
        Subsequence subsequence = new Subsequence();
        String s = "abc";
        String t = "ahbgdc";
        boolean expected = true;

        boolean actual = subsequence.isSubsequence(s, t);

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Should return true when both strings are the same")
    void isSubsequenceWhenBothStringsAreSame() {
        Subsequence subsequence = new Subsequence();
        String s = "ahbgdc";
        String t = "ahbgdc";
        boolean expected = true;

        boolean actual = subsequence.isSubsequence(s, t);

        assertEquals(expected, actual);
    }

}